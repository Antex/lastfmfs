require 'fusefs'
require 'rubygems'
require 'json'
require 'net/http'
require 'memcache'
require 'open-uri'

class LastfmDir
	attr_reader :artists, :dbfile
	
	def initialize( artists = ["metallica", "korn"] )
		@dbfile = "/home/antex/lastfm.db"
		
		if File.exists?(@dbfile)
			load_db()
		else
			@artists = artists
			save_db()
		end
		
		@cache = MemCache.new 'localhost:11211'
	end
	
	def save_db()
		f = File.open(@dbfile, 'w')
		f.puts @artists.to_json
		f.close
	end
	
	def load_db()
		buffer = File.open(@dbfile, 'r').read
		@artists = JSON.parse(buffer)
	end
	
	def httpget(url)
		#puts 'httpget ' + url
		
		unless @cache.get(url).nil?
			#puts 'returned from cache'
			return @cache.get(url)
		end
		
		resp = Net::HTTP.get_response(URI.parse(url))
		data = resp.body
		@cache.set(url, data, 86400)
		return data
	end
	
	def gettopalbums(artist)
		#puts 'gettopalbums ' + artist 
		url = "http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=" + URI::encode(artist) + "&api_key=b25b959554ed76058ac220b7b2e0a026&format=json"
		data = httpget(url)
		result = JSON.parse(data)

		if result.has_key? 'Error'
			raise "web service error"
		end
		
		return result["topalbums"]["album"]
	end
	
	def gettracks(artist, album)
		url = "http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=b25b959554ed76058ac220b7b2e0a026&artist=" + URI::encode(artist) + "&album=" + URI::encode(album) + "&format=json"
		data = httpget(url)
		result = JSON.parse(data)

		if result.has_key? 'Error'
			raise "web service error"
		end
		
		return result["album"]["tracks"]["track"]
	end
	
	def contents(path)
		#puts "contents: " + path
		path = path.split(File::SEPARATOR)
		p path
		
		if(path.count == 0)
			artists
		else 
			if(path.count == 2)
				albums = gettopalbums path[1]
				#p albums
				albums_array = []
				albums.each{ |album| albums_array.push(album["name"].gsub("/", "_")) }
				return albums_array
			else
				if(path.count == 3)
					tracks = gettracks path[1], path[2]
					#p tracks
					tracks_array = []
					tracks.each{ |track| tracks_array.push(track["name"].gsub("/", "_")) }
					return tracks_array
				else
					['no_defined']
				end
			end
		end
	end
	
	def directory?(path)
		#puts "directory?: " + path
		path = path.split(File::SEPARATOR)
		p path
		
		if(path.count == 2)
			if not @artists.include?(path[1])
				return false
			else 
				return true
			end
		end
		
		if(path.count == 3 or path.count == 4)
			return true
		end		
		
		return false
	end
	
	def can_mkdir?(path)
		#puts "can_mkdir?: " + path
		path = path.split(File::SEPARATOR)
		p path
		
		path.count == 2
	end
	
	def mkdir(path)
		path = path.split(File::SEPARATOR)
		@artists.push path[1]
		save_db()
	end
	
	def file?(path)
		#puts "file?: " + path
		path = path.split(File::SEPARATOR)
		p path
		
		if(path.count > 2)
			return true
		else
			return false
		end
	end
	
	def read_file(path)
		"Hello, World!\n"
	end
end
	
lastfmdir = LastfmDir.new
FuseFS.set_root( lastfmdir )

FuseFS.mount_under ARGV.shift
FuseFS.run
